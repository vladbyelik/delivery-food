// import { initializeApp } from "https://www.gstatic.com/firebasejs/9.6.1/firebase-app.js";

const firebaseConfig = {
  apiKey: "AIzaSyCs-dOv931tPJORVJaS-ZxYBDO255UA3gc",
  authDomain: "d-food2.firebaseapp.com",
  databaseURL: "https://d-food2-default-rtdb.firebaseio.com",
  projectId: "d-food2",
  storageBucket: "d-food2.appspot.com",
  messagingSenderId: "865394882176",
  appId: "1:865394882176:web:df4e5412ff929286e9d865"
};

firebase.initializeApp(firebaseConfig);

//

const cardsRestaurants = document.querySelector('.cards-restaurants');
const cardsMenu = document.querySelector('.cards-menu');



//

let login = localStorage.getItem('deliveryFood');

const getData = async (key) => {
  const data = await firebase.database().ref().child(key).once('value')
  return data.val();
}

const createCardRestaurant = (restaurant) => {

  const { image, name, price, stars, products, kitchen, time_of_delivery } = restaurant;

  const card = `
    <a class="card card-restaurant" data-products="${products}">
      <img src="${image}" alt="image" class="card-image"/>
      <div class="card-text">
        <div class="card-heading">
          <h3 class="card-title">${name}</h3>
          <span class="card-tag tag">${time_of_delivery}</span>
        </div>
        <div class="card-info">
          <div class="rating">
            ${stars}
          </div>
          <div class="price">От $${price}</div>
          <div class="category">${kitchen}</div>
        </div>
      </div>
    </a>
  `;

  cardsRestaurants.insertAdjacentHTML('beforeend', card);
}

const createCardGood = ({ description, image, name, price, id }) => {

  const card = document.createElement('div');
  card.className = 'card';

  const good = `
    <img src="${image}" alt="image" class="card-image" />
    <div class="card-text">

      <div class="card-heading">
        <h3 class="card-title card-title-reg">${name}</h3>
      </div>

      <div class="card-info">
        <div class="ingredients">${description}</div>
      </div>

      <div class="card-buttons">
        <button class="button button-primary button-add-cart" id="${id}">
          <span class="button-card-text">В корзину</span>
          <span class="button-cart-svg"></span>
        </button>
        <strong class="card-price-bold card-price">$${price}</strong>
      </div>
    </div>`;
  
  card.insertAdjacentHTML('beforeend', good);
  cardsMenu.insertAdjacentElement('beforeend', card);
}

const init = () => {
  getData('partners').then(data => {
    cardsRestaurants.firstElementChild.remove();
    data.forEach(item => {
      createCardRestaurant(item);
    })
  })

  
}

$('.swiper-wrapper').slick({
  autoplay: true,
  autoplaySpeed: 2000,
  arrows: false
});


init();